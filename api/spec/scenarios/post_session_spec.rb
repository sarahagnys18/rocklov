
describe "POST /sessions" do
  context "login com sucesso" do
    before(:all) do
      payload = { email: "betao@hotmail.com", password: "pwd123" }
      @result = Sessions.new.login(payload)
    end

    it "valida status code" do
      expect(@result.code).to eql 200
    end

    it "valida id do usuário" do
      expect(@result.parsed_response["_id"].length).to eql 24
    end
  end

  # examples = [
  #   {
  #     title: "Senha incorreta",
  #     payload: { email: "raquel@gmail.com", password: "senha1234" },
  #     code: 401,
  #     error: "Unauthorized",
  #   },
  #   {
  #     title: "Usuario nao existe",
  #     payload: { email: "raquel1@gmail.com", password: "senha123" },
  #     code: 401,
  #     error: "Unauthorized",
  #   },
  #   {
  #     title: "Email em branco",
  #     payload: { email: "", password: "senha1234" },
  #     code: 412,
  #     error: "required email",
  #   },
  #   {
  #     title: "Sem o campo email",
  #     payload: { password: "senha1234" },
  #     code: 412,
  #     error: "required email",
  #   },
  #   {
  #     title: "Senha em branco",
  #     payload: { email: "raquel1@gmail.com", password: "" },
  #     code: 412,
  #     error: "required password",
  #   },
  #   {
  #     title: "Sem o campo senha",
  #     payload: { email: "raquel1@gmail.com" },
  #     code: 412,
  #     error: "required password",
  #   },
  # ]

  examples = Helpers::get_fixture("login")
  
  examples.each do |e|
    context "#{e[:title]}" do
      before(:all) do
        @result = Sessions.new.login(e[:payload])
      end
  
      it "valida status code #{e[:code]}" do
        expect(@result.code).to eql e[:code]
      end
  
      it "valida mensagem de erro" do
        expect(@result.parsed_response["error"]).to eql e[:error]
      end
    end
  end
end
