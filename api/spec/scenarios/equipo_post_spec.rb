describe "POST /equipos" do
  before(:all) do
    payload = { email: "to@mate.com", password: "pwd123" }
    result = Sessions.new.login(payload)
    @user_id = result.parsed_response["_id"]
  end

  context "novo equipo" do
    before(:all) do
      payload = { 
      thumbnail: Helpers::get_thumb("kramer.jpg"), 
      name: "kramer", 
      category: "Cordas",
      price: "299",
    }
      #puts @result
      @result = Equipos.new.create(payload, @user_id)
      Mongodb.new.remove_equipo(payload[:name], @user_id)
    end
    it "deve retornar 200" do
      expect(@result.code).to eql 200
    end
  end

  context "não autorizado" do
    before(:all) do
      payload = { 
        thumbnail: Helpers::get_thumb("baixo.jpg"),
        name: "Contra baixo", 
        category: "Cordas", 
        price: "59", 
      }
      #puts @result
      @result = Equipos.new.create(payload, nil)
    end
    it "deve retornar 401" do
      expect(@result.code).to eql 401
    end
  end
end
