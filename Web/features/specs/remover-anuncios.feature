#language: pt

Funcionalidade: Remover Anuncios
    Sendo um anunciante que possui um equipamento indesejado
    Quero poder remover esse anuncio
    Para que eu possa manter meu Dashboard atualizado

    Contexto: Login
        * Login com "spider@hotmail.com" e "senha123"

    
    Cenario: Remover um anuncio
        Dado que eu tenho um anuncio indesejado:
            | thumb     | fender-sb.jpg |
            | nome      | Fender Strato |
            | categoria | Cordas        |
            | preco     | 200           |
        Quando eu solicito a exclusao desse item
            E confirmo a exclusao
        Entao nao devo ver esse item no meu Dashboard

    @temp
    Cenario: Desistir da exclusao
        Dado que eu tenho um anuncio indesejado:
            | thumb     | conga.jpg |
            | nome      | Conga     |
            | categoria | Outras    |
            | preco     | 100       |
        Quando eu solicito a exclusao desse item
            Mas nao confirmo a solicitacao
        Entao esse item deve permanecer no meu Dashboard
