#language: pt

Funcionalidade: Login
    Sendo um usuário cadastrado
    Quero acessar o sistema da Rocklov
    Para que eu possa anunciar meus equipamentos musicais

@login
    Cenario: Login do usuário
        Dado que acesso a página principal
        Quando submeto minhas credenciais com "sah@gmail.com" e "senha123"
        Então sou redirecionado para o Dashboard

    Esquema do Cenário: Tentar logar     
        Dado que acesso a página principal
        Quando submeto minhas credenciais com "<email_input>" e "<senha_input>"
        Então vejo a mensagem de alerta: "<mensagem_output>"

        Exemplos:
            | email_input      | senha_input | mensagem_output                 |
            | sah@gmail.com    | senha14     | Usuário e/ou senha inválidos.   |
            | raquel@gmail.com | senha14     | Usuário e/ou senha inválidos.   |
            | sah*gmail.com    | senha14     | Oops. Informe um email válido!  |
            |                  | senha14     | Oops. Informe um email válido!  |
            | sah@gmail.com    |             | Oops. Informe sua senha secreta!|
  